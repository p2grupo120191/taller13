
bin/prueba: obj/prueba.o obj/Ejemplo.o obj/automovil.o obj/estudiante.o obj/libro.o obj/slabAllocator.o obj/objetoEjemplo.o
	gcc -Wall -g -pthread obj/Ejemplo.o obj/automovil.o obj/estudiante.o obj/libro.o obj/slabAllocator.o obj/prueba.o obj/objetoEjemplo.o -o bin/prueba -lpng

obj/prueba.o: src/prueba.c
	gcc -Wall -g -c -I include/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -g -c -I include/ src/Ejemplo.c -o obj/Ejemplo.o

obj/automovil.o: src/automovil.c
	gcc -Wall -g -c -I include/ src/automovil.c -o obj/automovil.o

obj/estudiante.o: src/estudiante.c
	gcc -Wall -g -c -I include/ src/estudiante.c -o obj/estudiante.o

obj/objetoEjemplo.o: src/objetoEjemplo.c
	gcc -Wall -g -c -I include/ src/objetoEjemplo.c -o obj/objetoEjemplo.o

obj/libro.o: src/libro.c
	gcc -Wall -g -c -I include/ src/libro.c -o obj/libro.o

obj/slabAllocator.o: src/slabAllocator.c
	gcc -Wall -g -c -I include/ src/slabAllocator.c -o obj/slabAllocator.o
#agregue las reglas que necesite
.PHONY: clean
clean:
	rm bin/* obj/*.o

run:
	./bin/prueba

	
