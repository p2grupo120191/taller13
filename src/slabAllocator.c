#include "slaballoc.h"
#include "objetos.h"


SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
    //Inicialización, asignación
    SlabAlloc *ptr=(SlabAlloc *)malloc(sizeof(SlabAlloc));
    ptr->nombre=nombre;
    ptr->mem_ptr=(void *)malloc(tamano_objeto*TAMANO_CACHE);
    ptr->tamano_cache=TAMANO_CACHE;
    ptr->constructor=constructor;
    ptr->destructor=destructor;

    for(int i=0;i<tamano_objeto*TAMANO_CACHE;i+=tamano_objeto){
      constructor(ptr->mem_ptr+i,tamano_objeto);
    }

    ptr->cantidad_en_uso=0;
    ptr->tamano_objeto=tamano_objeto;
    Slab* punteroSlab=(Slab *)malloc(sizeof(Slab)*TAMANO_CACHE);

    for(int j=0;j<TAMANO_CACHE;j++){
      Slab cache;

      cache.ptr_data = ptr->mem_ptr+(j*tamano_objeto);
      cache.ptr_data_old = ptr->mem_ptr+(j*tamano_objeto);
      cache.status = DISPONIBLE;
      sem_init(&(cache.semaforo),0,1);
      *(punteroSlab+j)=cache;
    }
    ptr->slab_ptr=punteroSlab;

    return ptr;
}

void *obtener_cache(SlabAlloc *alloc, int crecer){
  if(alloc == NULL){
    return NULL;
  }

  Slab *ptr = alloc->slab_ptr;

  int i = 0;
  //P(s)
  for (i = 0; i< alloc->tamano_cache;i++){
    if(ptr[i].status == DISPONIBLE){
      sem_wait(&(ptr[i].semaforo));
      ptr[i].status = EN_USO;
      alloc->cantidad_en_uso +=1;
      sem_post(&(ptr[i].semaforo));
      return ptr[i].ptr_data;
    }
  }
  //V(S)
  //sem_post(&(alloc->slab_ptr->semaforo));
  return NULL;
}

void devolver_cache(SlabAlloc *alloc, void *obj){
  Slab *ptr= alloc->slab_ptr;

  for(int i =0;i<alloc->tamano_cache;i++){
    if (obj==ptr[i].ptr_data){
      sem_wait(&(ptr[i].semaforo));
      ptr[i].ptr_data=NULL;
      ptr[i].status= DISPONIBLE;
      alloc->cantidad_en_uso -=1;
      alloc->destructor(alloc->mem_ptr + alloc->tamano_objeto*i, alloc->tamano_objeto);
      sem_post(&(ptr[i].semaforo));
      break;
    }
  }
}

void destruir_cache(SlabAlloc *alloc){
  if (alloc->cantidad_en_uso == 0){
    int i = 0;
    for (i = 0; i < alloc->tamano_cache; i++){
      alloc->destructor(alloc->mem_ptr + alloc->tamano_objeto*i, alloc->tamano_objeto);
    }
  free(alloc->slab_ptr);
  free(alloc->mem_ptr);
  alloc = NULL;
  free(alloc);
  }
}

void stats_cache(SlabAlloc *cache){
  SlabAlloc cache1 = *cache;
  printf("\nNombre del SlabAlloc:\t%s\n", cache1.nombre);
  printf("Cantidad de slabs:\t%d\n", cache1.tamano_cache);
  printf("Cantidad de slabs en uso:\t%d\n", cache1.cantidad_en_uso);
  printf("Tamaño de objeto:\t%ld\n\n", cache1.tamano_objeto);
  printf("Dirección de cache: %p\n\n",cache1.mem_ptr);

  Slab *ptr = cache->slab_ptr;
  for (int i = 0; i< TAMANO_CACHE; i++){
    if(ptr[i].status == DISPONIBLE){
      printf("Direccion ptr[%d].ptr_data: <%p>    <DISPONIBLE>,   ptr[%d].ptr_data_old: <%p>\n", i, ptr[i].ptr_data, i, ptr[i].ptr_data_old);
    } else {
      printf("Direccion ptr[%d].ptr_data: <%p>    <EN_USO>,   ptr[%d].ptr_data_old: <%p>\n", i, ptr[i].ptr_data, i, ptr[i].ptr_data_old);
    }
  }
}
