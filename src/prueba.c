#include "slaballoc.h"
#include "objetos.h"
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>



typedef struct arghilo
{
	void*alloc;
	int idhilo;
	
}argv_hilo;

void *funcion_hilo(void *args ){
	argv_hilo *argv=(argv_hilo*)args;
	void *objetos[1000]={0};
	SlabAlloc *buffalloc=(SlabAlloc * )argv->alloc;
	for(int j=0;j<1000;j++){
        void *obj =obtener_cache(buffalloc,1);
		objeto_ejemplo *cast=(objeto_ejemplo*)obj;
        cast->id_hilo = argv->idhilo;
        cast->data[0] = argv->idhilo * 0;
        cast->data[1] = argv->idhilo * 1;
        cast->data[2] = argv->idhilo * 2;
        objetos[j] = obj;

	}

	for(int k=700;k<1000;k++){
		devolver_cache(buffalloc,objetos[k]);
	}

	for(int x=0;x<700;x++){
        objeto_ejemplo *cast1 = (objeto_ejemplo *)objetos[x];
        if(cast1->id_hilo!=argv->idhilo||cast1->data[0] != argv->idhilo* 0|| cast1->data[1] != argv->idhilo* 1|| cast1->data[2] != argv->idhilo* 2){

            printf("Valor esperado  %d, valor encontrado %d",(cast1->id_hilo),argv->idhilo);//OJO FALTA DE COMPLETAR EL VALOR ESPERADO VS VALOR ENCONTRADO

		}

	}
	char *res="exito";
	return (void*)res;

}




int main(int argc,char* argv[]){

	if(argc!=5){
		printf("Uso: ./prueba -n <# de hilos> -k <# de objetos>\n");
		exit(-1);
	}
	SlabAlloc *alloc = crear_cache("PruebaAlloc",sizeof(objeto_ejemplo),crear_objetoEjemplo,eliminar_objetoEjemplo);

	pthread_t id_hilos[atoi(argv[2])];
	argv_hilo args[atoi(argv[2])];

	for(int i=0;i<atoi(argv[2]);i++){
		args[i].alloc=alloc;
		args[i].idhilo=i;
		pthread_create(&id_hilos[i],NULL,funcion_hilo,&args[i] );
	}
	for(int i = 0; i<atoi(argv[2]);i++){
		pthread_join(id_hilos[i],NULL);
	}
	
	return 0;
}









